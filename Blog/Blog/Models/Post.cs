﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        [Key] 
	    public int Id { set; get; }
	    public String Title { set; get; }
	    public string Body { set; get; }
    }
    public class BlogDbContext: DbContext
	{
		public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Commnents { set; get; }
	}
}