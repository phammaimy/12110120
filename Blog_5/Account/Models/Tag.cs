﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Account.Models
{
    public class Tag
    {
        [Required]
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự từ 10 đến 100", MinimumLength = 10)]
        public String content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}