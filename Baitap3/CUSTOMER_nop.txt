
CUSTOMER

MSSV: 12110233
Tôi chưa có con, nhưng tôi có cháu 5 tháng tuổi, bé tên Ngọc Anh, tôi rất thích trẻ con, nên thường quan tâm đến những thông tin liên quan đến dinh dưỡng, 
đồ chơi, giáo dục(về thông tin trường lớp, cách giáo dục trẻ), đặc biệt tôi rất quan tâm về tin tức những vụ
bạo hành ngược đãi trẻ em, tôi muốn tìm những cửa hàng bán đồ chơi không độc hại cho trẻ, những cửa hàng thức ăn
 đảm bảo ăn toàn vệ sinh thực phẩm mà vừa có đủ dinh dưỡng cho bé, những môi trường vui chơi, giải trí lành mạnh cho bé
cùng những thông tin tham khảo về các trường từ cấp mẫu giáo đến những cấp lớn hơn, đảm bảo cho cháu tôi phát triển 
ở môi trường hoàn thiện nhất. Trang web của bạn có đầy đủ những thông tin mà tôi cần.
Ngoài ra, tôi nghĩ web của bạn nên có thông tin thêm về cách nuôi dạy trẻ, những kinh nghiệm đúc kết được từ các
 bà mẹ đã từ nuôi dạy con theo cách riêng của họ, bằng cách bạn nên có một trang blog để chúng tôi có thể chia 
sẻ kinh nghiệm cùng nhau để giúp các bé trưởng thành tốt hơn.
Tôi rất quan tâm đến bé, nên muốn tìm hiểu thật nhều về những thông tin trên, tôi muốn biết nhiều cửa cửa hàng đồ chơi,
 thức ăn, khu vui chơi, các trường công lập, bán công tư thục trong và ngoài nước để có thêm nhiều sự lựa chọn cho 
bé Ngọc Anh, vị vậy tôi nghĩ việc thiết kế một trang web riêng dành cho phu huynh tham khảo thông tin về bé thì thật 
cần thiết, và sẽ tôi là chắc chắn sẽ truy cập vào web này thường xuyên sau khi nó được hoàn tất.
