﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webcuoiky.Models
{
    public class CommentCatHealth
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập đúng Ngày/Tháng/Năm")]
        public System.DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int CatHealthID { set; get; }
        public virtual CatHealth CatHealth { set; get; }

        public int UserProfileUserId { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }
}