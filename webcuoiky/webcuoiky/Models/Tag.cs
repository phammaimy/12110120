﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webcuoiky.Models
{
    public class Tag
    {
        [Required]
        public int TagID { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự từ 10 đến 100", MinimumLength = 10)]
        public String Content { get; set; }


        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<CatMart> CatMarts { get; set; }

        public virtual ICollection<CatHealth> CatHealths { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<MommentCute> MommentCutes { get; set; }
        public virtual ICollection<TinTuc> TinTucs { get; set; }
        public virtual ICollection<CafeMeo> CafeMeo { get; set; }
    }
}