﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webcuoiky.Models
{
    public class Comment
    {

        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập đúng Ngày/Tháng/Năm")]
        public System.DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }





        public int Loai { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }

        public int CatMartID { set; get; }

        public virtual CatMart CatMart { set; get; }

        public int CatHealthID { set; get; }
        public virtual CatHealth CatHealth { set; get; }

        public int BlogID { set; get; }
        public virtual Blog Blog { set; get; }

        public int MommentCuteID { set; get; }

        public virtual MommentCute MommentCute { set; get; }
    }
}