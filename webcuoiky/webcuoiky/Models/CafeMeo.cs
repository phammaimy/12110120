﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webcuoiky.Models
{
    public class CafeMeo
    {
        public int ID { set; get; }
        [Display(Name = "CafeMeo")]

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(200, ErrorMessage = "Số lượng kí tự nằm trong khoảng từ 20 đến 200", MinimumLength = 20)]
        public string Title { set; get; }


        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(111111111, ErrorMessage = "Số lượng ký tự tối thiểu là 20 ký tự", MinimumLength = 20)]
        public string Body { set; get; }


        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ!")]
        public DateTime DateCreated { set; get; }


        public String Image { set; get; }

        public int UserProfileUserId { set; get; }
        public virtual UserProfile UserProfile { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<CommentCafeMeo> CommentCafeMeos { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}