﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webcuoiky.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự nằm trong khoảng từ 20 đến 100", MinimumLength = 20)]
        public String Title { set; get; }


        [Required(ErrorMessage = "Không được bỏ trống")]
        public String Body { set; get; }


        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ!")]
        public DateTime DateCreated { set; get; }




        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}