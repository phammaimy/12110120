﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace webcuoiky.Models
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext()
            : base("BlogDbContext")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<CatMart> CatMarts { get; set; }

        public DbSet<CatHealth> CatHealths { get; set; }

        public DbSet<Blog> Blogs { get; set; }

        public DbSet<MommentCute> MommentCutes { get; set; }

        public DbSet<CafeMeo> CafeMeos { get; set; }

        public DbSet<TinTuc> TinTucs { get; set; }

        public DbSet<CommentCafeMeo> CommentCafeMeos { get; set; }

        public DbSet<CommentTinTuc> CommentTinTucs { get; set; }

        public DbSet<CommentCatMart> CommentCatMarts { get; set; }

        public DbSet<CommentCatHealth> CommentCatHealths { get; set; }

        public DbSet<CommentBlog> CommentBlogs { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public virtual ICollection<Post> Posts { set; get; }
        public virtual ICollection<CafeMeo> CafeMeos { set; get; }
        public virtual ICollection<CommentCafeMeo> CommentCafeMeos { set; get; }
      
       
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
