namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentTinTucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        TinTucID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TinTucs", t => t.TinTucID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.TinTucID)
                .Index(t => t.UserProfileUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentTinTucs", new[] { "UserProfileUserId" });
            DropIndex("dbo.CommentTinTucs", new[] { "TinTucID" });
            DropForeignKey("dbo.CommentTinTucs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.CommentTinTucs", "TinTucID", "dbo.TinTucs");
            DropTable("dbo.CommentTinTucs");
        }
    }
}
