namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan91 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentCatHealths",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CatHealthID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CatHealths", t => t.CatHealthID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.CatHealthID)
                .Index(t => t.UserProfileUserId);
            
            AddColumn("dbo.CatHealths", "Image", c => c.String());
            AddColumn("dbo.CatHealths", "UserProfileUserId", c => c.Int(nullable: false));
            AddForeignKey("dbo.CatHealths", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: false);
            CreateIndex("dbo.CatHealths", "UserProfileUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentCatHealths", new[] { "UserProfileUserId" });
            DropIndex("dbo.CommentCatHealths", new[] { "CatHealthID" });
            DropIndex("dbo.CatHealths", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.CommentCatHealths", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.CommentCatHealths", "CatHealthID", "dbo.CatHealths");
            DropForeignKey("dbo.CatHealths", "UserProfileUserId", "dbo.UserProfile");
            DropColumn("dbo.CatHealths", "UserProfileUserId");
            DropColumn("dbo.CatHealths", "Image");
            DropTable("dbo.CommentCatHealths");
        }
    }
}
