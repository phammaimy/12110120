namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TinTucs", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TinTucs", "Image");
        }
    }
}
