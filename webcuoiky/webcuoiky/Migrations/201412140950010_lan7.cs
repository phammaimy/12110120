namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CatMarts", "Image", c => c.String());
            AddColumn("dbo.CatMarts", "UserProfileUserId", c => c.Int(nullable: false));
            AddForeignKey("dbo.CatMarts", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: false);
            CreateIndex("dbo.CatMarts", "UserProfileUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CatMarts", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.CatMarts", "UserProfileUserId", "dbo.UserProfile");
            DropColumn("dbo.CatMarts", "UserProfileUserId");
            DropColumn("dbo.CatMarts", "Image");
        }
    }
}
