namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentCafeMeos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CafeMeoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CafeMeos", t => t.CafeMeoID, cascadeDelete: true)
                .Index(t => t.CafeMeoID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentCafeMeos", new[] { "CafeMeoID" });
            DropForeignKey("dbo.CommentCafeMeos", "CafeMeoID", "dbo.CafeMeos");
            DropTable("dbo.CommentCafeMeos");
        }
    }
}
