// <auto-generated />
namespace webcuoiky.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan91 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan91));
        
        string IMigrationMetadata.Id
        {
            get { return "201412141138427_lan91"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
