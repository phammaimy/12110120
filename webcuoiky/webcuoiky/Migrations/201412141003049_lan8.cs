namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentCatMarts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CatMartID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CatMarts", t => t.CatMartID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.CatMartID)
                .Index(t => t.UserProfileUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentCatMarts", new[] { "UserProfileUserId" });
            DropIndex("dbo.CommentCatMarts", new[] { "CatMartID" });
            DropForeignKey("dbo.CommentCatMarts", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.CommentCatMarts", "CatMartID", "dbo.CatMarts");
            DropTable("dbo.CommentCatMarts");
        }
    }
}
