namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CommentCafeMeos", "CatHealth_ID", "dbo.CatHealths");
            DropIndex("dbo.CommentCafeMeos", new[] { "CatHealth_ID" });
            DropColumn("dbo.CommentCafeMeos", "CatHealth_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CommentCafeMeos", "CatHealth_ID", c => c.Int());
            CreateIndex("dbo.CommentCafeMeos", "CatHealth_ID");
            AddForeignKey("dbo.CommentCafeMeos", "CatHealth_ID", "dbo.CatHealths", "ID");
        }
    }
}
