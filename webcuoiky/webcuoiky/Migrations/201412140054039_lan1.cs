namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CafeMeos", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CafeMeos", "Image");
        }
    }
}
