namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MommentCutes", "Image", c => c.String());
            AddColumn("dbo.MommentCutes", "UserProfileUserId", c => c.Int(nullable: false));
            AddForeignKey("dbo.MommentCutes", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: false);
            CreateIndex("dbo.MommentCutes", "UserProfileUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MommentCutes", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.MommentCutes", "UserProfileUserId", "dbo.UserProfile");
            DropColumn("dbo.MommentCutes", "UserProfileUserId");
            DropColumn("dbo.MommentCutes", "Image");
        }
    }
}
