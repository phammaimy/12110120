namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentBlogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        BlogID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Blogs", t => t.BlogID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.BlogID)
                .Index(t => t.UserProfileUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentBlogs", new[] { "UserProfileUserId" });
            DropIndex("dbo.CommentBlogs", new[] { "BlogID" });
            DropForeignKey("dbo.CommentBlogs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.CommentBlogs", "BlogID", "dbo.Blogs");
            DropTable("dbo.CommentBlogs");
        }
    }
}
