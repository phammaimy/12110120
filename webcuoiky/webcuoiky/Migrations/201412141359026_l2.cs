namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommentCafeMeos", "CatHealth_ID", c => c.Int());
            AddForeignKey("dbo.CommentCafeMeos", "CatHealth_ID", "dbo.CatHealths", "ID");
            CreateIndex("dbo.CommentCafeMeos", "CatHealth_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentCafeMeos", new[] { "CatHealth_ID" });
            DropForeignKey("dbo.CommentCafeMeos", "CatHealth_ID", "dbo.CatHealths");
            DropColumn("dbo.CommentCafeMeos", "CatHealth_ID");
        }
    }
}
