namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommentCafeMeos", "UserProfileUserId", c => c.Int(nullable: false));
            AddForeignKey("dbo.CommentCafeMeos", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: false);
            CreateIndex("dbo.CommentCafeMeos", "UserProfileUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CommentCafeMeos", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.CommentCafeMeos", "UserProfileUserId", "dbo.UserProfile");
            DropColumn("dbo.CommentCafeMeos", "UserProfileUserId");
        }
    }
}
