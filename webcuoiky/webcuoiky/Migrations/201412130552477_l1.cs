namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Loai = c.Int(nullable: false),
                        PostID = c.Int(nullable: false),
                        CatMartID = c.Int(nullable: false),
                        CatHealthID = c.Int(nullable: false),
                        BlogID = c.Int(nullable: false),
                        MommentCuteID = c.Int(nullable: false),
                        TinTuc_ID = c.Int(),
                        CafeMeo_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.CatMarts", t => t.CatMartID, cascadeDelete: true)
                .ForeignKey("dbo.CatHealths", t => t.CatHealthID, cascadeDelete: true)
                .ForeignKey("dbo.Blogs", t => t.BlogID, cascadeDelete: true)
                .ForeignKey("dbo.MommentCutes", t => t.MommentCuteID, cascadeDelete: true)
                .ForeignKey("dbo.TinTucs", t => t.TinTuc_ID)
                .ForeignKey("dbo.CafeMeos", t => t.CafeMeo_ID)
                .Index(t => t.PostID)
                .Index(t => t.CatMartID)
                .Index(t => t.CatHealthID)
                .Index(t => t.BlogID)
                .Index(t => t.MommentCuteID)
                .Index(t => t.TinTuc_ID)
                .Index(t => t.CafeMeo_ID);
            
            CreateTable(
                "dbo.CatMarts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.CatHealths",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MommentCutes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TinTucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.CafeMeos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Post_ID })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.Post_ID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Post_ID);
            
            CreateTable(
                "dbo.TagCatMarts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        CatMart_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.CatMart_ID })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.CatMarts", t => t.CatMart_ID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.CatMart_ID);
            
            CreateTable(
                "dbo.CatHealthTags",
                c => new
                    {
                        CatHealth_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CatHealth_ID, t.Tag_TagID })
                .ForeignKey("dbo.CatHealths", t => t.CatHealth_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.CatHealth_ID)
                .Index(t => t.Tag_TagID);
            
            CreateTable(
                "dbo.BlogTags",
                c => new
                    {
                        Blog_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Blog_ID, t.Tag_TagID })
                .ForeignKey("dbo.Blogs", t => t.Blog_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.Blog_ID)
                .Index(t => t.Tag_TagID);
            
            CreateTable(
                "dbo.MommentCuteTags",
                c => new
                    {
                        MommentCute_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MommentCute_ID, t.Tag_TagID })
                .ForeignKey("dbo.MommentCutes", t => t.MommentCute_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.MommentCute_ID)
                .Index(t => t.Tag_TagID);
            
            CreateTable(
                "dbo.TinTucTags",
                c => new
                    {
                        TinTuc_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TinTuc_ID, t.Tag_TagID })
                .ForeignKey("dbo.TinTucs", t => t.TinTuc_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.TinTuc_ID)
                .Index(t => t.Tag_TagID);
            
            CreateTable(
                "dbo.CafeMeoTags",
                c => new
                    {
                        CafeMeo_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CafeMeo_ID, t.Tag_TagID })
                .ForeignKey("dbo.CafeMeos", t => t.CafeMeo_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.CafeMeo_ID)
                .Index(t => t.Tag_TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CafeMeoTags", new[] { "Tag_TagID" });
            DropIndex("dbo.CafeMeoTags", new[] { "CafeMeo_ID" });
            DropIndex("dbo.TinTucTags", new[] { "Tag_TagID" });
            DropIndex("dbo.TinTucTags", new[] { "TinTuc_ID" });
            DropIndex("dbo.MommentCuteTags", new[] { "Tag_TagID" });
            DropIndex("dbo.MommentCuteTags", new[] { "MommentCute_ID" });
            DropIndex("dbo.BlogTags", new[] { "Tag_TagID" });
            DropIndex("dbo.BlogTags", new[] { "Blog_ID" });
            DropIndex("dbo.CatHealthTags", new[] { "Tag_TagID" });
            DropIndex("dbo.CatHealthTags", new[] { "CatHealth_ID" });
            DropIndex("dbo.TagCatMarts", new[] { "CatMart_ID" });
            DropIndex("dbo.TagCatMarts", new[] { "Tag_TagID" });
            DropIndex("dbo.TagPosts", new[] { "Post_ID" });
            DropIndex("dbo.TagPosts", new[] { "Tag_TagID" });
            DropIndex("dbo.TinTucs", new[] { "UserProfileUserId" });
            DropIndex("dbo.Comments", new[] { "CafeMeo_ID" });
            DropIndex("dbo.Comments", new[] { "TinTuc_ID" });
            DropIndex("dbo.Comments", new[] { "MommentCuteID" });
            DropIndex("dbo.Comments", new[] { "BlogID" });
            DropIndex("dbo.Comments", new[] { "CatHealthID" });
            DropIndex("dbo.Comments", new[] { "CatMartID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.CafeMeoTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.CafeMeoTags", "CafeMeo_ID", "dbo.CafeMeos");
            DropForeignKey("dbo.TinTucTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.TinTucTags", "TinTuc_ID", "dbo.TinTucs");
            DropForeignKey("dbo.MommentCuteTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.MommentCuteTags", "MommentCute_ID", "dbo.MommentCutes");
            DropForeignKey("dbo.BlogTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.BlogTags", "Blog_ID", "dbo.Blogs");
            DropForeignKey("dbo.CatHealthTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.CatHealthTags", "CatHealth_ID", "dbo.CatHealths");
            DropForeignKey("dbo.TagCatMarts", "CatMart_ID", "dbo.CatMarts");
            DropForeignKey("dbo.TagCatMarts", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.TagPosts", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.TinTucs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "CafeMeo_ID", "dbo.CafeMeos");
            DropForeignKey("dbo.Comments", "TinTuc_ID", "dbo.TinTucs");
            DropForeignKey("dbo.Comments", "MommentCuteID", "dbo.MommentCutes");
            DropForeignKey("dbo.Comments", "BlogID", "dbo.Blogs");
            DropForeignKey("dbo.Comments", "CatHealthID", "dbo.CatHealths");
            DropForeignKey("dbo.Comments", "CatMartID", "dbo.CatMarts");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.CafeMeoTags");
            DropTable("dbo.TinTucTags");
            DropTable("dbo.MommentCuteTags");
            DropTable("dbo.BlogTags");
            DropTable("dbo.CatHealthTags");
            DropTable("dbo.TagCatMarts");
            DropTable("dbo.TagPosts");
            DropTable("dbo.CafeMeos");
            DropTable("dbo.TinTucs");
            DropTable("dbo.MommentCutes");
            DropTable("dbo.Blogs");
            DropTable("dbo.CatHealths");
            DropTable("dbo.Tags");
            DropTable("dbo.CatMarts");
            DropTable("dbo.Comments");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
