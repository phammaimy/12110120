namespace webcuoiky.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CafeMeos", "UserProfileUserId", c => c.Int(nullable: false));
            AddForeignKey("dbo.CafeMeos", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: true);
            CreateIndex("dbo.CafeMeos", "UserProfileUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CafeMeos", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.CafeMeos", "UserProfileUserId", "dbo.UserProfile");
            DropColumn("dbo.CafeMeos", "UserProfileUserId");
        }
    }
}
