﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    public class CommentCatMartController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CommentCatMart/

        public ActionResult Index()
        {
            var commentcatmarts = db.CommentCatMarts.Include(c => c.CatMart);
            return View(commentcatmarts.ToList());
        }

        //
        // GET: /CommentCatMart/Details/5

        public ActionResult Details(int id = 0)
        {
            CommentCatMart commentcatmart = db.CommentCatMarts.Find(id);
            if (commentcatmart == null)
            {
                return HttpNotFound();
            }
            return View(commentcatmart);
        }

        //
        // GET: /CommentCatMart/Create

        public ActionResult Create()
        {
            ViewBag.CatMartID = new SelectList(db.CatMarts, "ID", "Title");
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /CommentCatMart/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentCatMart commentcatmart, int IDPost)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                commentcatmart.UserProfileUserId = userid;

                commentcatmart.CatMartID = IDPost;
                commentcatmart.DateCreated = DateTime.Now;
                db.CommentCatMarts.Add(commentcatmart);
                db.SaveChanges();
                return RedirectToAction("Details/" + IDPost, "CafeMeo");
                //return RedirectToAction("Index");
            }

            ViewBag.CatMartID = new SelectList(db.CatMarts, "ID", "Title", commentcatmart.CatMartID);
            
            return View(commentcatmart);
        }

        //
        // GET: /CommentCatMart/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CommentCatMart commentcatmart = db.CommentCatMarts.Find(id);
            if (commentcatmart == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatMartID = new SelectList(db.CatMarts, "ID", "Title", commentcatmart.CatMartID);
          //  ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentcatmart.UserProfileUserId);
            return View(commentcatmart);
        }

        //
        // POST: /CommentCatMart/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentCatMart commentcatmart)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commentcatmart).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CatMartID = new SelectList(db.CatMarts, "ID", "Title", commentcatmart.CatMartID);
           // ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentcatmart.UserProfileUserId);
            return View(commentcatmart);
        }

        //
        // GET: /CommentCatMart/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommentCatMart commentcatmart = db.CommentCatMarts.Find(id);
            if (commentcatmart == null)
            {
                return HttpNotFound();
            }
            return View(commentcatmart);
        }

        //
        // POST: /CommentCatMart/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CommentCatMart commentcatmart = db.CommentCatMarts.Find(id);
            db.CommentCatMarts.Remove(commentcatmart);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}