﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    public class CommentBlogController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CommentBlog/

        public ActionResult Index()
        {
            var commentblogs = db.CommentBlogs.Include(c => c.Blog).Include(c => c.UserProfile);
            return View(commentblogs.ToList());
        }

        //
        // GET: /CommentBlog/Details/5

        public ActionResult Details(int id = 0)
        {
            CommentBlog commentblog = db.CommentBlogs.Find(id);
            if (commentblog == null)
            {
                return HttpNotFound();
            }
            return View(commentblog);
        }

        //
        // GET: /CommentBlog/Create

        public ActionResult Create()
        {
            ViewBag.BlogID = new SelectList(db.Blogs, "ID", "Title");
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /CommentBlog/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentBlog commentblog, int IDPost)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                commentblog.UserProfileUserId = userid;

                commentblog.BlogID = IDPost;
                commentblog.DateCreated = DateTime.Now;
                db.CommentBlogs.Add(commentblog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BlogID = new SelectList(db.Blogs, "ID", "Title", commentblog.BlogID);
           // ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentblog.UserProfileUserId);
            return View(commentblog);
        }

        //
        // GET: /CommentBlog/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CommentBlog commentblog = db.CommentBlogs.Find(id);
            if (commentblog == null)
            {
                return HttpNotFound();
            }
            ViewBag.BlogID = new SelectList(db.Blogs, "ID", "Title", commentblog.BlogID);
           // ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentblog.UserProfileUserId);
            return View(commentblog);
        }

        //
        // POST: /CommentBlog/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentBlog commentblog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commentblog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BlogID = new SelectList(db.Blogs, "ID", "Title", commentblog.BlogID);
            //ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentblog.UserProfileUserId);
            return View(commentblog);
        }

        //
        // GET: /CommentBlog/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommentBlog commentblog = db.CommentBlogs.Find(id);
            if (commentblog == null)
            {
                return HttpNotFound();
            }
            return View(commentblog);
        }

        //
        // POST: /CommentBlog/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CommentBlog commentblog = db.CommentBlogs.Find(id);
            db.CommentBlogs.Remove(commentblog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}