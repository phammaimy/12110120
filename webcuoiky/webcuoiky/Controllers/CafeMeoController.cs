﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    [Authorize]
    public class CafeMeoController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CafeMeo/

        public ActionResult Index()
        {
            return View(db.CafeMeos.ToList());
        }

        //
        // GET: /CafeMeo/Details/5

        public ActionResult Details(int id = 0)
        {
            CafeMeo cafemeo = db.CafeMeos.Find(id);
            if (cafemeo == null)
            {
                return HttpNotFound();
            }
            //cái dưới thêm
            ViewData["idpost"] = id;
            return View(cafemeo);
        }

        //
        // GET: /CafeMeo/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CafeMeo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CafeMeo cafemeo)
        {
            if (ModelState.IsValid)
            {
                db.CafeMeos.Add(cafemeo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cafemeo);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FileUpload(CafeMeo cafemeo, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                cafemeo.UserProfileUserId = userid;
                cafemeo.DateCreated = DateTime.Now;
                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);

                    string physicalPath = Server.MapPath("~/images/" + ImageName);

                    file.SaveAs(physicalPath);

                    cafemeo.Image = ImageName;
                    db.CafeMeos.Add(cafemeo);
                    db.SaveChanges();
                }
                else
                {

                    cafemeo.Image = "NH0.jpg";
                    db.CafeMeos.Add(cafemeo);
                    db.SaveChanges();
                }
                //Display records
                return RedirectToAction("Index");
            }

            return View(cafemeo);
        }




        //
        // GET: /CafeMeo/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CafeMeo cafemeo = db.CafeMeos.Find(id);
            if (cafemeo == null)
            {
                return HttpNotFound();
            }
            return View(cafemeo);
        }

        //
        // POST: /CafeMeo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CafeMeo cafemeo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cafemeo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cafemeo);
        }

        //
        // GET: /CafeMeo/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CafeMeo cafemeo = db.CafeMeos.Find(id);
            if (cafemeo == null)
            {
                return HttpNotFound();
            }
            return View(cafemeo);
        }

        //
        // POST: /CafeMeo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CafeMeo cafemeo = db.CafeMeos.Find(id);
            db.CafeMeos.Remove(cafemeo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}