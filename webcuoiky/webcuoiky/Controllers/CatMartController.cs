﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    [Authorize]
    public class CatMartController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CatMart/

        public ActionResult Index()
        {
            return View(db.CatMarts.ToList());
        }

        //
        // GET: /CatMart/Details/5

        public ActionResult Details(int id = 0)
        {
            CatMart catmart = db.CatMarts.Find(id);
            if (catmart == null)
            {

                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(catmart);
        }

        //
        // GET: /CatMart/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CatMart/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CatMart catmart)
        {
            if (ModelState.IsValid)
            {
                db.CatMarts.Add(catmart);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(catmart);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FileUpload(CatMart catmart, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                catmart.UserProfileUserId = userid;
                catmart.DateCreated = DateTime.Now;
                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);

                    string physicalPath = Server.MapPath("~/images/" + ImageName);

                    file.SaveAs(physicalPath);

                    catmart.Image = ImageName;
                    db.CatMarts.Add(catmart);
                    db.SaveChanges();
                }
                else
                {

                    catmart.Image = "NH0.jpg";
                    db.CatMarts.Add(catmart);
                    db.SaveChanges();
                }
                //Display records
                return RedirectToAction("Index");
            }

            return View(catmart);
        }
        //
        // GET: /CatMart/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CatMart catmart = db.CatMarts.Find(id);
            if (catmart == null)
            {
                return HttpNotFound();
            }
            return View(catmart);
        }

        //
        // POST: /CatMart/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CatMart catmart)
        {
            if (ModelState.IsValid)
            {
                db.Entry(catmart).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(catmart);
        }

        //
        // GET: /CatMart/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CatMart catmart = db.CatMarts.Find(id);
            if (catmart == null)
            {
                return HttpNotFound();
            }
            return View(catmart);
        }

        //
        // POST: /CatMart/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatMart catmart = db.CatMarts.Find(id);
            db.CatMarts.Remove(catmart);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}