﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    [Authorize]
    public class TinTucController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /TinTuc/

        public ActionResult Index()
        {
           
            return View(db.TinTucs.ToList());
        }

        //
        // GET: /TinTuc/Details/5

        public ActionResult Details(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(tintuc);
        }

        //
        // GET: /TinTuc/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /TinTuc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TinTuc tintuc)
        {
            if (ModelState.IsValid)
            {
                db.TinTucs.Add(tintuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tintuc.UserProfileUserId);
            return View(tintuc);
        }

        //
        // GET: /TinTuc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tintuc.UserProfileUserId);
            return View(tintuc);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FileUpload(TinTuc tintuc, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                tintuc.UserProfileUserId = userid;
                tintuc.DateCreated = DateTime.Now;
                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);

                    string physicalPath = Server.MapPath("~/images/" + ImageName);

                    file.SaveAs(physicalPath);

                    tintuc.Image = ImageName;
                    db.TinTucs.Add(tintuc);
                    db.SaveChanges();
                }
                else
                {

                    tintuc.Image = "NH0.jpg";
                    db.TinTucs.Add(tintuc);
                    db.SaveChanges();
                }
                //Display records
                return RedirectToAction("Index");
            }

            return View(tintuc);
        }
        //
        // POST: /TinTuc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TinTuc tintuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tintuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tintuc.UserProfileUserId);
            return View(tintuc);
        }

        //
        // GET: /TinTuc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            return View(tintuc);
        }

        //
        // POST: /TinTuc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            db.TinTucs.Remove(tintuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}