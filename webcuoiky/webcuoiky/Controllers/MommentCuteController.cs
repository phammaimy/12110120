﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    [Authorize]
    public class MommentCuteController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /MommentCute/

        public ActionResult Index()
        {
            return View(db.MommentCutes.ToList());
        }

        //
        // GET: /MommentCute/Details/5

        public ActionResult Details(int id = 0)
        {
            MommentCute mommentcute = db.MommentCutes.Find(id);
            if (mommentcute == null)
            {
                return HttpNotFound();
            }
            return View(mommentcute);
        }

        //
        // GET: /MommentCute/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MommentCute/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MommentCute mommentcute)
        {
            if (ModelState.IsValid)
            {
                db.MommentCutes.Add(mommentcute);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mommentcute);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FileUpload(MommentCute mommentcute, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                mommentcute.UserProfileUserId = userid;
                mommentcute.DateCreated = DateTime.Now;
                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);

                    string physicalPath = Server.MapPath("~/images/" + ImageName);

                    file.SaveAs(physicalPath);

                    mommentcute.Image = ImageName;
                    db.MommentCutes.Add(mommentcute);
                    db.SaveChanges();
                }
                else
                {

                    mommentcute.Image = "NH0.jpg";
                    db.MommentCutes.Add(mommentcute);
                    db.SaveChanges();
                }
                //Display records
                return RedirectToAction("Index");
            }

            return View(mommentcute);
        }
        //
        // GET: /MommentCute/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MommentCute mommentcute = db.MommentCutes.Find(id);
            if (mommentcute == null)
            {
                return HttpNotFound();
            }
            return View(mommentcute);
        }

        //
        // POST: /MommentCute/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MommentCute mommentcute)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mommentcute).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mommentcute);
        }

        //
        // GET: /MommentCute/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MommentCute mommentcute = db.MommentCutes.Find(id);
            if (mommentcute == null)
            {
                return HttpNotFound();
            }
            return View(mommentcute);
        }

        //
        // POST: /MommentCute/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MommentCute mommentcute = db.MommentCutes.Find(id);
            db.MommentCutes.Remove(mommentcute);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}