﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    public class CommentTinTucController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CommentTinTuc/

        public ActionResult Index()
        {
            var commenttintucs = db.CommentTinTucs.Include(c => c.TinTuc);
            return View(commenttintucs.ToList());
        }

        //
        // GET: /CommentTinTuc/Details/5

        public ActionResult Details(int id = 0)
        {
            CommentTinTuc commenttintuc = db.CommentTinTucs.Find(id);
            if (commenttintuc == null)
            {
                return HttpNotFound();
            }
            return View(commenttintuc);
        }

        //
        // GET: /CommentTinTuc/Create

        public ActionResult Create()
        {
            ViewBag.TinTucID = new SelectList(db.TinTucs, "ID", "Title");
            return View();
        }

        //
        // POST: /CommentTinTuc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentTinTuc commenttintuc, int IDPost)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                commenttintuc.UserProfileUserId = userid;

                commenttintuc.TinTucID = IDPost;
                commenttintuc.DateCreated = DateTime.Now;
                db.CommentTinTucs.Add(commenttintuc);
                db.SaveChanges();
                return RedirectToAction("Details/" + IDPost, "TinTuc");
               // return RedirectToAction("Index");
            }

            ViewBag.TinTucID = new SelectList(db.TinTucs, "ID", "Title", commenttintuc.TinTucID);
            return View(commenttintuc);
        }

        //
        // GET: /CommentTinTuc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CommentTinTuc commenttintuc = db.CommentTinTucs.Find(id);
            if (commenttintuc == null)
            {
                return HttpNotFound();
            }
            ViewBag.TinTucID = new SelectList(db.TinTucs, "ID", "Title", commenttintuc.TinTucID);
            //ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commenttintuc.UserProfileUserId);
            return View(commenttintuc);
        }

        //
        // POST: /CommentTinTuc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentTinTuc commenttintuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commenttintuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TinTucID = new SelectList(db.TinTucs, "ID", "Title", commenttintuc.TinTucID);
            //ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commenttintuc.UserProfileUserId);
            return View(commenttintuc);
        }

        //
        // GET: /CommentTinTuc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommentTinTuc commenttintuc = db.CommentTinTucs.Find(id);
            if (commenttintuc == null)
            {
                return HttpNotFound();
            }
            return View(commenttintuc);
        }

        //
        // POST: /CommentTinTuc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CommentTinTuc commenttintuc = db.CommentTinTucs.Find(id);
            db.CommentTinTucs.Remove(commenttintuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}