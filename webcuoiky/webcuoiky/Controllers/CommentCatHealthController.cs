﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    public class CommentCatHealthController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CommentCatHealth/

        public ActionResult Index()
        {
            var commentcathealths = db.CommentCatHealths.Include(c => c.CatHealth);
            return View(commentcathealths.ToList());
        }

        //
        // GET: /CommentCatHealth/Details/5

        public ActionResult Details(int id = 0)
        {
            CommentCatHealth commentcathealth = db.CommentCatHealths.Find(id);
            if (commentcathealth == null)
            {
                return HttpNotFound();
            }
            return View(commentcathealth);
        }

        //
        // GET: /CommentCatHealth/Create

        public ActionResult Create()
        {
            ViewBag.CatHealthID = new SelectList(db.CatHealths, "ID", "Title");
           // ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /CommentCatHealth/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentCatHealth commentcathealth,int IDPost)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                commentcathealth.UserProfileUserId = userid;

                commentcathealth.CatHealthID = IDPost;
                commentcathealth.DateCreated = DateTime.Now;
                db.CommentCatHealths.Add(commentcathealth);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CatHealthID = new SelectList(db.CatHealths, "ID", "Title", commentcathealth.CatHealthID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentcathealth.UserProfileUserId);
            return View(commentcathealth);
        }

        //
        // GET: /CommentCatHealth/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CommentCatHealth commentcathealth = db.CommentCatHealths.Find(id);
            if (commentcathealth == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatHealthID = new SelectList(db.CatHealths, "ID", "Title", commentcathealth.CatHealthID);
            //ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentcathealth.UserProfileUserId);
            return View(commentcathealth);
        }

        //
        // POST: /CommentCatHealth/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentCatHealth commentcathealth)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commentcathealth).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CatHealthID = new SelectList(db.CatHealths, "ID", "Title", commentcathealth.CatHealthID);
            //ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", commentcathealth.UserProfileUserId);
            return View(commentcathealth);
        }

        //
        // GET: /CommentCatHealth/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommentCatHealth commentcathealth = db.CommentCatHealths.Find(id);
            if (commentcathealth == null)
            {
                return HttpNotFound();
            }
            return View(commentcathealth);
        }

        //
        // POST: /CommentCatHealth/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CommentCatHealth commentcathealth = db.CommentCatHealths.Find(id);
            db.CommentCatHealths.Remove(commentcathealth);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}