﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    public class CommentCafeMeoController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CommentCafeMeo/

        public ActionResult Index()
        {
            var commentcafemeos = db.CommentCafeMeos.Include(c => c.CafeMeo);
            return View(commentcafemeos.ToList());
        }

        //
        // GET: /CommentCafeMeo/Details/5

        public ActionResult Details(int id = 0)
        {
            CommentCafeMeo commentcafemeo = db.CommentCafeMeos.Find(id);
            if (commentcafemeo == null)
            {
                return HttpNotFound();
            }
            return View(commentcafemeo);
        }

        //
        // GET: /CommentCafeMeo/Create

        public ActionResult Create()
        {
            ViewBag.CafeMeoID = new SelectList(db.CafeMeos, "ID", "Title");
            return View();
        }

        //
        // POST: /CommentCafeMeo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentCafeMeo commentcafemeo, int IDPost)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                commentcafemeo.UserProfileUserId = userid;

                commentcafemeo.CafeMeoID = IDPost;
                commentcafemeo.DateCreated = DateTime.Now;

                db.CommentCafeMeos.Add(commentcafemeo);
                db.SaveChanges();
                return RedirectToAction("Details/" + IDPost, "CafeMeo");
                //return RedirectToAction("Index");
            }

            ViewBag.CafeMeoID = new SelectList(db.CafeMeos, "ID", "Title", commentcafemeo.CafeMeoID);
            return View(commentcafemeo);
        }

        //
        // GET: /CommentCafeMeo/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CommentCafeMeo commentcafemeo = db.CommentCafeMeos.Find(id);
            if (commentcafemeo == null)
            {
                return HttpNotFound();
            }
            ViewBag.CafeMeoID = new SelectList(db.CafeMeos, "ID", "Title", commentcafemeo.CafeMeoID);
            return View(commentcafemeo);
        }

        //
        // POST: /CommentCafeMeo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentCafeMeo commentcafemeo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commentcafemeo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CafeMeoID = new SelectList(db.CafeMeos, "ID", "Title", commentcafemeo.CafeMeoID);
            return View(commentcafemeo);
        }

        //
        // GET: /CommentCafeMeo/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommentCafeMeo commentcafemeo = db.CommentCafeMeos.Find(id);
            if (commentcafemeo == null)
            {
                return HttpNotFound();
            }
            return View(commentcafemeo);
        }

        //
        // POST: /CommentCafeMeo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CommentCafeMeo commentcafemeo = db.CommentCafeMeos.Find(id);
            db.CommentCafeMeos.Remove(commentcafemeo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}