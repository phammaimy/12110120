﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcuoiky.Models;

namespace webcuoiky.Controllers
{
    [Authorize]
    public class CatHealthController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CatHealth/

        public ActionResult Index()
        {
            return View(db.CatHealths.ToList());
        }

        //
        // GET: /CatHealth/Details/5

        public ActionResult Details(int id = 0)
        {
            CatHealth cathealth = db.CatHealths.Find(id);
            if (cathealth == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(cathealth);
        }

        //
        // GET: /CatHealth/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CatHealth/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CatHealth cathealth)
        {
            if (ModelState.IsValid)
            {
                db.CatHealths.Add(cathealth);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cathealth);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FileUpload(CatHealth cathealth, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                cathealth.UserProfileUserId = userid;
                cathealth.DateCreated = DateTime.Now;
                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);

                    string physicalPath = Server.MapPath("~/images/" + ImageName);

                    file.SaveAs(physicalPath);

                    cathealth.Image = ImageName;
                    db.CatHealths.Add(cathealth);
                    db.SaveChanges();
                }
                else
                {

                    cathealth.Image = "NH0.jpg";
                    db.CatHealths.Add(cathealth);
                    db.SaveChanges();
                }
                //Display records
                return RedirectToAction("Index");
            }

            return View(cathealth);
        }

        //
        // GET: /CatHealth/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CatHealth cathealth = db.CatHealths.Find(id);
            if (cathealth == null)
            {
                return HttpNotFound();
            }
            return View(cathealth);
        }

        //
        // POST: /CatHealth/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CatHealth cathealth)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cathealth).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cathealth);
        }

        //
        // GET: /CatHealth/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CatHealth cathealth = db.CatHealths.Find(id);
            if (cathealth == null)
            {
                return HttpNotFound();
            }
            return View(cathealth);
        }

        //
        // POST: /CatHealth/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatHealth cathealth = db.CatHealths.Find(id);
            db.CatHealths.Remove(cathealth);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}